# Create database user for publishing event

## MySQL
* CREATE DATABASE publisher_db CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
* CREATE USER 'publisher_usr'@'%' IDENTIFIED BY 'zaq12wsx';
* GRANT ALL PRIVILEGES ON publisher_db.* TO 'publisher_usr'@'%' WITH GRANT OPTION;
* FLUSH PRIVILEGES;

## Oracle
* CREATE USER publisher_usr IDENTIFIED BY zaq12wsx;
* GRANT connect, resource, create view TO publisher_usr;
* ALTER USER publisher_usr quota unlimited on USERS;