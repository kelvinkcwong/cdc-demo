package com.example.cdc.publisher.service;

import com.example.cdc.publisher.repo.EventHelloRepository;
import com.example.cdc.publisher.repo.entity.EventHelloEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class HelloProcessor {
    private final EventHelloRepository repository;
    private final HelloService helloService;
    private final ObjectMapper objectMapper;

    @Transactional
    public String sayHello(String name) throws JsonProcessingException {
        String greeting = helloService.sayHello(name);
        Instant now = Instant.now();
        Map payload = Map.of("message", greeting);

        EventHelloEntity event = EventHelloEntity.builder()
                .oid(UUID.randomUUID().toString())
                .payload(objectMapper.writeValueAsString(payload))
                .createdDateTime(now)
                .build();
        repository.save(event);

        return greeting;
    }
}

