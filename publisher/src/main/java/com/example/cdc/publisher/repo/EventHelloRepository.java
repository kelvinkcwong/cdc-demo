package com.example.cdc.publisher.repo;

import com.example.cdc.publisher.repo.entity.EventHelloEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventHelloRepository extends JpaRepository<EventHelloEntity, String> {
}
