package com.example.cdc.publisher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CdcPublisherApplication {

	public static void main(String[] args) {
		SpringApplication.run(CdcPublisherApplication.class, args);
	}

}
