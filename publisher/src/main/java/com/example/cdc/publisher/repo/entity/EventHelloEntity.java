package com.example.cdc.publisher.repo.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "EVENT_HELLO")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EventHelloEntity {

    @Id
    @Column(name = "OID")
    String oid;

    @Column(name = "PAYLOAD")
    String payload;

    @Column(name = "CREATED_DATETIME")
    Instant createdDateTime;
}
