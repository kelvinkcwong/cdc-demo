package com.example.cdc.publisher.controller;

import com.example.cdc.publisher.service.HelloProcessor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
public class HelloController {

    private final HelloProcessor helloProcessor;

    @GetMapping("/hello")
    @ResponseBody
    public String sayHello(@RequestParam(name = "name") String name) throws Exception {
        log.info("sayHello, name = {}", name);
        return helloProcessor.sayHello(name);
    }
}
