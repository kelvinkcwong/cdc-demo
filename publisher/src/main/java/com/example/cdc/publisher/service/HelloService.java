package com.example.cdc.publisher.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@Service
@RequiredArgsConstructor
public class HelloService {
    private final ObjectMapper objectMapper;

    public String sayHello(String name) throws JsonProcessingException {
        Instant now = Instant.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss O").withZone(ZoneId.systemDefault());
        return String.format("Hello %s, current time is %s", name, formatter.format(now));
    }
}

