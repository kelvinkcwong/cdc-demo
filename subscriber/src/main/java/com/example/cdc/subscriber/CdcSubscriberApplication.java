package com.example.cdc.subscriber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CdcSubscriberApplication {

	public static void main(String[] args) {
		SpringApplication.run(CdcSubscriberApplication.class, args);
	}

}
