package com.example.cdc.subscriber.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class DebeziumConfiguration {

    @Value("${server.port}")
    private Integer port;

    private static final String schema = "publisher_db";

    @Bean
    public io.debezium.config.Configuration customConnector() {
        log.info("Create debezium customConnector...");

        var commonConfiguration = io.debezium.config.Configuration.create()
                .with("database.server.id", port)
                .with("database.server.name", String.format("subscriber.%s", port))
                .with("offset.storage", "org.apache.kafka.connect.storage.FileOffsetBackingStore")
                .with("offset.storage.file.filename", String.format("./target/offsets-%s.dat", port))
                .with("offset.flush.interval.ms", "60000")
                .with("include.schema.changes", "false")
                .with("database.history", "io.debezium.relational.history.FileDatabaseHistory")
                .with("database.history.file.filename", String.format("./target/dbhistory-%s.dat", port))
                // subscribe change on EVENT_* tables
                .with("table.include.list", String.format("^(%s.event_).*$", schema))
                // subscribe to inserted records only
                .with("transforms", "filter")
                .with("transforms.filter.type", "io.debezium.transforms.Filter")
                .with("transforms.filter.language", "jsr223.groovy")
                .with("transforms.filter.condition", "value.op == 'c'")
                ;

        return commonConfiguration
                .with("name", "publisher_db-mysql-connector")
                .with("connector.class", "io.debezium.connector.mysql.MySqlConnector")
                .with("database.hostname", "localhost")
                .with("database.port", "3306")
                .with("database.user", "cdc_usr")
                .with("database.password", "zaq12wsx")
                .with("database.allowPublicKeyRetrieval", "true")
                .with("database.dbname", "publisher_db")
                // specify db schema to subscribe
                .with("database.include.list", schema)
                .build();

//        return commonConfiguration
//                .with("name", "publisher_db-oracle-connector")
//                .with("connector.class", "io.debezium.connector.oracle.OracleConnector")
//                .with("database.hostname", "localhost")
//                .with("database.port", "1521")
//                .with("database.user", "cdc_usr")
//                .with("database.password", "zaq12wsx")
//                .with("database.dbname", "ORCLCDB")
//                .with("snapshot.mode", "schema_only")
//                // specify db schema to subscribe
//                .with("schema.include.list", schema)
//                .build();
    }
}