# Create database user for debezium connector

## ***Setup for MySQL***
### Create user with required privileges
* CREATE DATABASE cdc_db CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
* CREATE USER 'cdc_usr'@'%' IDENTIFIED BY 'zaq12wsx';
* GRANT REPLICATION SLAVE, REPLICATION CLIENT, RELOAD ON \*.\* TO 'cdc_usr'@'%';
* FLUSH PRIVILEGES;

## ***Setup for Oracle***
### SSH to Oracle console and create the following directory
* /opt/oracle/oradata/ORCLCDB/recovery_area

### Login to Oracle as with sysdba role
* CONNECT / AS SYSDBA

### Enable archive log mode
* alter system set db_recovery_file_dest_size = 10G;
* alter system set db_recovery_file_dest = '/opt/oracle/oradata/ORCLCDB/recovery_area' scope=spfile;
* shutdown immediate
* startup mount
* alter database archivelog;
* alter database open;
* archive log list;
* -- Should show "Database log mode              Archive Mode"
* alter database add supplemental log data;

### Create user with required privileges
* CREATE TABLESPACE logminer_tbs DATAFILE '/opt/oracle/oradata/ORCLCDB/logminer_tbs.dbf' SIZE 25M REUSE AUTOEXTEND ON MAXSIZE UNLIMITED;
* CREATE USER cdc_usr IDENTIFIED BY zaq12wsx DEFAULT TABLESPACE logminer_tbs QUOTA UNLIMITED ON logminer_tbs;
* GRANT CREATE SESSION TO cdc_usr ;
* GRANT SET CONTAINER TO cdc_usr ;
* GRANT SELECT ON V_$DATABASE to cdc_usr ;
* GRANT FLASHBACK ANY TABLE TO cdc_usr ;
* GRANT SELECT ANY TABLE TO cdc_usr ;
* GRANT SELECT_CATALOG_ROLE TO cdc_usr ;
* GRANT EXECUTE_CATALOG_ROLE TO cdc_usr ;
* GRANT SELECT ANY TRANSACTION TO cdc_usr ;
* GRANT LOGMINING TO cdc_usr ;
* GRANT CREATE TABLE TO cdc_usr ;
* GRANT LOCK ANY TABLE TO cdc_usr ;
* GRANT CREATE SEQUENCE TO cdc_usr ;
* GRANT EXECUTE ON DBMS_LOGMNR TO cdc_usr ;
* GRANT EXECUTE ON DBMS_LOGMNR_D TO cdc_usr ;
* GRANT SELECT ON V_$LOG TO cdc_usr ;
* GRANT SELECT ON V_$LOG_HISTORY TO cdc_usr ;
* GRANT SELECT ON V_$LOGMNR_LOGS TO cdc_usr ;
* GRANT SELECT ON V_$LOGMNR_CONTENTS TO cdc_usr ;
* GRANT SELECT ON V_$LOGMNR_PARAMETERS TO cdc_usr ;
* GRANT SELECT ON V_$LOGFILE TO cdc_usr ;
* GRANT SELECT ON V_$ARCHIVED_LOG TO cdc_usr ;
* GRANT SELECT ON V_$ARCHIVE_DEST_STATUS TO cdc_usr ;
* GRANT SELECT ON V_$TRANSACTION TO cdc_usr ; 